/* 
 * File:   config_bits.h
 * Author: flpoi
 *
 * Created on 22 f�vrier 2019, 15:02
 */

#ifndef CONFIG_BITS_H
#define CONFIG_BITS_H
// CONFIG
#pragma config FOSC = HS // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF // FLASH Program Memory Code Protection bits
#pragma config BOREN = ON // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF // Low Voltage In-Circuit Serial Programming Enable bit
#pragma config CPD = OFF // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = ON // FLASH Program Memory Write Enable
#endif /* CONFIG_BITS_H */