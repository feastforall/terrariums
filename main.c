/* 
 * File:   main.c
 * Author: flpoi
 *
 * Created on 22 f�vrier 2019, 14:57
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "config_bits.h"
#include "pic16f877.h"
#define _XTAL_FREQ 4000000 // clock 4Mhz
#define VREF_plus 5.0f // vref + = Vcc tension d'alimentation
#define VREF_moins 0.0f // vref - = 0V
#define PLEINE_ECH 1024 // convertisseur 10 bits. pleine echelle = 2^10 = 1024
volatile float tension = 0.0f;

double temperature;
const int TEMP_MIN = 22;
const int TEMP_MAX = 38;


void incrementTemperature() {
    double temp_temperature = temperature + 4;
    temperature = ( temp_temperature >= TEMP_MAX) ? TEMP_MAX : temp_temperature;
}

void decrementTemperature() {
    double temp_temperature = temperature - 4;
    temperature = (temp_temperature <= TEMP_MIN) ? TEMP_MIN : temp_temperature;
}

double getTemperature() {
    unsigned long ValeurADC = 0L;
    ADCON1 = 0b10001110; // right justified vref+=Vdd vref- = Vss RA0 an autres entr�es
    ADCON0 = 0b01000001; // Fosc/8 RA0 stop x AD on

    ADCON0bits.GO_DONE = 1; // lancer laconversion
    while (ADCON0bits.GO_DONE == 1); // attente fin de conversion
    ValeurADC = ADRESH << 8; // lecture valeur convertie MSB
    ValeurADC += ADRESL; // + LSB => valeur 16 bits
    tension = (VREF_plus - VREF_moins) * ValeurADC / PLEINE_ECH; // calcul de la tension
    double temp_degres = ((tension+0.1)*1000) / 125;
    return temp_degres;
}

void incrementOrDecrement() {
    if (RC1 == 0) {
        incrementTemperature();
        turnOnLed();
    } else if (RC0 == 0) {
        decrementTemperature();
        turnOnLed();
    }
}

void regulateTemperature(){
    double temp_degres = getTemperature();
    
    if (RD1 == 0 && temp_degres < temperature -1) {
        RD1 = 1;
        RD0 = 0;    
    } else if (RD1 == 1 && temp_degres >= temperature) {
        RD1 = 0;
        RD0 = 0; 
    }else if(RD1 == 0 && temp_degres > temperature +1){
        RD1 = 0;
        RD0 = 1; 
    } else if (RD0 == 1 && temp_degres <= temperature) {
        RD1 = 0;
        RD0 = 0;
    }
}

void turnOnLed() {
    int temperature_level = (temperature - TEMP_MIN) / 4;
    switch (temperature_level) {
        case 0:
            RC3 = 1;
            RC4 = 0;
            RC5 = 0;
            RC6 = 0;
            RC7 = 0;
            break;
        case 1:
            RC3 = 0;
            RC4 = 1;
            RC5 = 0;
            RC6 = 0;
            RC7 = 0;
            break;
        case 2:
            RC3 = 0;
            RC4 = 0;
            RC5 = 1;
            RC6 = 0;
            RC7 = 0;
            break;
        case 3:
            RC3 = 0;
            RC4 = 0;
            RC5 = 0;
            RC6 = 1;
            RC7 = 0;
            break;
        case 4:
            RC3 = 0;
            RC4 = 0;
            RC5 = 0;
            RC6 = 0;
            RC7 = 1;
            break;
    }
}

void main(void) {
    TRISA = 0xFF; //port A tout en entree doc p 111
    TRISC = 0b00000011;
    TRISD = 0b00000000;
    temperature = 30;
    __delay_us(100);
    turnOnLed();
    while (1) {
        incrementOrDecrement();
        regulateTemperature();
        __delay_ms(500);
    }
}